class AnimalFactory {
    Animal getRndAnimal(boolean isOdd, String type){
        if(type.equalsIgnoreCase("elephant")){
            return new Elephant(isOdd);
        }
        if(type.equalsIgnoreCase("donkey")){
            return new Donkey(isOdd);
        }
        if(type.equalsIgnoreCase("chicken")){
            return new Chicken(isOdd);
        }
        return null;
    }
}
