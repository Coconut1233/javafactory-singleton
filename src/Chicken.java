public class Chicken implements Animal {
    private boolean isOdd;

    Chicken(boolean isOdd){
        this.isOdd = isOdd;
    }
    @Override
    public String toString() {
        return "Chicken ("+isOdd+")";
    }
}
