public class Donkey implements Animal {
    private boolean isOdd;

    Donkey(boolean isOdd){
        this.isOdd = isOdd;
    }
    @Override
    public String toString() {
        return "Donkey ("+isOdd+")";
    }
}
