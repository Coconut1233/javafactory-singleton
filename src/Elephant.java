public class Elephant implements Animal {
    private boolean isOdd;

    Elephant(boolean isOdd){
        this.isOdd = isOdd;
    }
    @Override
    public String toString() {
        return "Elephant ("+isOdd+")";
    }
}
