class EvenOdd {
    private boolean ret;
    private static EvenOdd evinst;

    private EvenOdd(){
        ret = false;
    }

    static EvenOdd getInstance(){
        if(evinst == null){
            evinst = new EvenOdd();
        }
        return evinst;
    }
    boolean getOdd() {
        ret = !ret;
        return ret;
    }
}
