public class Main {
    public static void main(String[] args) {
        AnimalFactory af = new AnimalFactory();
        EvenOdd ev = EvenOdd.getInstance();
        for(int i = 0; i < 10; i++){
            Animal a = af.getRndAnimal(ev.getOdd(), "elephant");
            System.out.println(a.toString());
        }
    }
}
